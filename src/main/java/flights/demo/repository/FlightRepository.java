package flights.demo.repository;

import flights.demo.model.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface FlightRepository  extends CrudRepository<Flight, Integer> {

}
