package flights.demo.controllers;


import flights.demo.model.Flight;
import flights.demo.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Map;

@RestController
public class TestController {

    @Autowired
    FlightService flightService;



    @GetMapping("/test")
    public Flight sayHello() {
        Flight f = new Flight();
        f.setDepart( new Date());
        f.setArrival( new Date());
        f.setHome("Krakow");
        f.setDestination("Malta");
        f.setNonStop(true);
        return f;
    }

    @CrossOrigin
    @GetMapping("/flights")
    public Iterable<Flight> getFlights() {
        return flightService.getAllFlights();
    }

    @CrossOrigin
    @PostMapping(value = "/flights", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addFlights( @RequestBody Flight flight) {
        flightService.addFlight(flight);
    }


}
