package flights.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlightsbackApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlightsbackApplication.class, args);
	}

}
