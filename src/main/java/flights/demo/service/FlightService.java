package flights.demo.service;

import flights.demo.model.Flight;
import flights.demo.repository.FlightRepository;
import org.springframework.stereotype.Service;

@Service
public class FlightService {

    FlightRepository flightRepository;

    public FlightService(FlightRepository flightRepository) {
        this.flightRepository = flightRepository;
    }

    public Iterable<Flight> getAllFlights() {
        return flightRepository.findAll();
    }

    public void addFlight(Flight flight) {
        flightRepository.save(flight);
    }
}
